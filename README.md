# Accomodations Manager #

This is an example of an accomodations service (manager) showing a LOS table (= Length of Stay pricing) for the delivered property.

### Requirements ###

* Laravel
* ORM

### Workflow considerations ###

In the first step, the accomodation manager (AM) is doing the calculation
related to the availabilities per day. Elements to consider are:

* the quantity
* is arrival allowed?
* is departure allowed on the exit day (if not, booking on that number of days also not possible)
* minimum stay (don't calculate before this)
* maximum stay (don't calculate after this)

After this list is obtain, AM is calculating the price for each amount of nights, 
and in this case for 1..3 persons.

First, a filteration over the pricing table is done in order to get
only the relevant results. This should match the following elements:

* period defined as from -> to
* the interval defined by minimum and maximum stay should include the calculated amount of booked nigths
* weekday (e.g. Sunday but not on Monday)
* persons

After the filtration is done, the calculation starts, taking into consideration
the number of persons, the number of extra persons (which have different tariff) and 
the duration of the stay.

For simplicity sake, AM considers the duration as a multiple of the tariff plus
the tariffs for one night, as in the example:

- given a tariff for 7 nights, 7 nights will match that one, 8 nights = tariff for 7 + 1x tariff for 1,
9 nights = tariff for 7 + 2 x tariff for 1 and so on.

### Notes ###

When there is no data, the returned result is 0.
An example of the dumped data can be found in _database_ folder.