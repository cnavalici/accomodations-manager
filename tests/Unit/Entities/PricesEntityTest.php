<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\Entities\Prices as PriceEntity;

class PricesEntityTest extends TestCase
{
    public function testContainsDateInterval()
    {
        $pe = (new PriceEntity)
            ->setPeriodFrom(new \DateTime('2019-01-03'))
            ->setPeriodTill(new \DateTime('2019-01-10'));


        $this->assertFalse($pe->containsDate(new \DateTime('2020-01-03')));
        $this->assertTrue($pe->containsDate(new \DateTime('2019-01-03')));
        $this->assertTrue($pe->containsDate(new \DateTime('2019-01-04')));
    }

    public function testAllowsByPersons()
    {
        $pe = (new PriceEntity)
            ->setPersons([3, 4, 5]);

        $this->assertFalse($pe->allowsByPersons(1));
        $this->assertTrue($pe->allowsByPersons(5));
    }

    public function testAllowsByWeekday()
    {
        $pe = (new PriceEntity)
            ->setWeekdays([0, 1, 2, 3]);

        $friday = new \DateTime('2019-06-14');
        $this->assertFalse($pe->allowsByWeekday($friday));

        $sunday = new \DateTime('2019-06-23');
        $this->assertTrue($pe->allowsByWeekday($sunday));
    }

    public function testIsExactDuration()
    {
        $pe = (new PriceEntity())
            ->setDuration(6);

        $this->assertFalse($pe->isExactDuration(1));
        $this->assertTrue($pe->isExactDuration(6));
        $this->assertFalse($pe->isExactDuration(8));
        $this->assertTrue($pe->isExactDuration(12));
    }

    public function testGetAmountForPersons()
    {
        $pe = (new PriceEntity())
            ->setAmount(100)
            ->setExtraPersonPrice(50)
            ->setPersons([2, 3, 4, 5]);

        $this->assertEquals(200, $pe->getAmountForPersons(2));
        $this->assertEquals(300, $pe->getAmountForPersons(4));
        $this->assertEquals(0, $pe->getAmountForPersons(100));
    }
}
