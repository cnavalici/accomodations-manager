<?php
/**
 * More likely for this particulary service, testing against db will be more appropiate
 * since a lot of variations are in the dataset and not in the processing logic (or at least, less)
 * This will also avoid a lot of mocking with different variations of Entity objects.
 */
namespace Tests\Unit;

use Tests\TestCase;
use App\Services\AvailabilityService;
use App\Entities\Availabilities as AvailEntity;

class AvailabilityServiceTest extends TestCase
{
    public function testCalculatePerStayDaysWithNoAvailability()
    {
        $em = $this->createEntityManager();

        $service = new AvailabilityService($em);
        $results = $service->calculatePerStayDays(new \DateTime('2017-01-01'));

        $this->assertEquals(AvailabilityService::MAX_STAY, count($results));
        $this->assertEquals(0, array_sum($results));
    }

    public function testCalculatePerStayDaysWithNotAllowed()
    {
        $avail_1 = new AvailEntity;
        $avail_1->setDate(new \DateTime('2017-01-01'));
        $avail_1->setArrivalAllowed(0);

        $em = $this->createEntityManager([$avail_1]);

        $service = new AvailabilityService($em);
        $results = $service->calculatePerStayDays(new \DateTime('2017-01-01'));

        $this->assertEquals(AvailabilityService::MAX_STAY, count($results));
        $this->assertEquals(0, array_sum($results));
    }

    public function testCalculatePerStayDaysWithSomeAvailability()
    {
        $em = $this->createEntityManager($this->generateAvailabilities());

        $service = new AvailabilityService($em);
        $results = $service->calculatePerStayDays(new \DateTime('2017-01-01'));

        $this->assertEquals(AvailabilityService::MAX_STAY, count($results));
        $this->assertEquals(AvailabilityService::MAX_STAY, array_sum($results));
    }

    private function createEntityManager($availabilities = [])
    {
        $repo = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $repo->method("findAll")->willReturn($availabilities);

        $em = $this->createMock(\Doctrine\ORM\EntityManager::class);
        $em->method("getRepository")->willReturn($repo);

        return $em;
    }

    private function generateAvailabilities()
    {
        $avails = [];

        for($a = 1; $a <= AvailabilityService::MAX_STAY + 1; $a++) {
            $avails[] = (new AvailEntity())
                ->setDate(new \DateTime(sprintf('2017-01-%02d',$a)))
                ->setArrivalAllowed(1)
                ->setDepartureAllowed(1)
                ->setQuantity(1)
                ->setMaxStay(21)
                ->setMinStay(1);
        }

        return $avails;

    }
}
