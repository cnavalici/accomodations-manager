<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Doctrine\ORM\EntityManagerInterface;
use App\Services\{AvailabilityService, PriceService};

class AmController extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    public function index(EntityManagerInterface $em) {
        $results = [];

        $year = new \DatePeriod(new \DateTime('2017-01-01'), new \DateInterval('P1D'), new \DateTime('2017-12-31'));
        foreach($year as $startDate) {
            $stayDays = (new AvailabilityService($em))->calculatePerStayDays($startDate);

            $ctService = (new PriceService($em, $startDate));
            $prices = $ctService->calculatePricePerStayDays($stayDays);

            $results[$startDate->format('Y-m-d')] = $prices;
        }

        return view('amanager')->with(['results' => $results ]);
    }
}
