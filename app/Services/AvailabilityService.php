<?php
namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;
use App\Entities\Availabilities;

class AvailabilityService
{
    const MAX_STAY = 21;

    private $em;
    private $avails;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $availsRaw = $em->getRepository(Availabilities::class)->findAll();

        $avails = [];
        foreach ($availsRaw as $av) {
            $avails[$av->getDate()->format('Y-m-d')] = $av;
        }

        $this->avails = $avails;
    }

    public function calculatePerStayDays(\DateTimeInterface $startDate): array
    {
        // if quantity = 0 or arrival_allowed = 0, then we have no results
        $results = array_fill(1, self::MAX_STAY, 0);

        $av = $this->avails[$startDate->format('Y-m-d')] ?? null;

        if (!$av || !$av->isAllowed()) {
            return $results;
        }

        // min / max stay into consideration
        $startDate = \DateTimeImmutable::createFromMutable($startDate);
        for ($d=$av->getMinStay(); $d<$av->getMinStay() + $av->getMaxStay(); $d++) {
             $exitDate = $startDate->add(new \DateInterval('P'.$d.'D'))->format('Y-m-d');
             $exitAv = $this->avails[$exitDate] ?? null;

             $results[$d] = (int)($exitAv && $exitAv->isDepartureAllowed());
        }

        return $results;
    }
}
