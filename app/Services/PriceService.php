<?php
namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\Criteria;
use App\Entities\Prices;

class PriceService
{
    const MAX_STAY = AvailabilityService::MAX_STAY;
    const MAX_PERS = 3;

    private $pricesRepo;
    private $startDate;
    private $pricesIntervals;

    public function __construct(EntityManagerInterface $em, \DateTimeInterface $startDate)
    {
        $this->pricesRepo = $em->getRepository(Prices::class);
        $this->em = $em;

        $this->startDate = \DateTimeImmutable::createFromMutable($startDate);
    }

    public function calculatePricePerStayDays(array $stayDaysChecked): array
    {
        $pricing = [];
        foreach ($stayDaysChecked as $dayNumber => $allowed) {
            for ($pers = 1; $pers <= self::MAX_PERS; $pers++) {
                $pricing[$dayNumber][$pers] = $allowed ? $this->calculatePrice($dayNumber, $pers) : 0;
            }
        }

        return $pricing;
    }

    public function calculatePrice(int $dayNumber, int $persons): int
    {
        $priceIntervals = $this->getPricesIntervals();
        $day = $this->startDate->add(new \DateInterval('P'.($dayNumber - 1).'D'));

        $filteredPriceIntervals = [];
        foreach ($priceIntervals as $p) {
            if ($p->containsDate($this->startDate) &&
                $p->allowsByPersons($persons) &&
                $p->allowsByStay($dayNumber) &&
                $p->allowsByWeekday($day)
            ) {
                $filteredPriceIntervals[$p->getMinimumStay()] = $p;
            }
        }

        if (!$filteredPriceIntervals) {
            return 0;
        }

        $closestMinStay = max(array_keys($filteredPriceIntervals));
        $xtimes = ($dayNumber - ($dayNumber % $closestMinStay)) / $closestMinStay;
        $rtimes = $dayNumber - $closestMinStay;

        $p1 = $filteredPriceIntervals[$closestMinStay]->getAmountForPersons($persons);
        $p2 = $filteredPriceIntervals[1]->getAmountForPersons($persons);

        return $p1 * $xtimes + $p2 * $rtimes;
    }

    protected function getPricesIntervals()
    {
        if ($this->pricesIntervals) {
            return $this->pricesIntervals;
        }

        $endDate = $this->startDate->add(new \DateInterval('P'.self::MAX_STAY.'D'));

        $prices = $this->pricesRepo->findAll();

        $criteriaLow = Criteria::create()
            ->where(Criteria::expr()->lte("periodFrom", $this->startDate))
            ->setMaxResults(1)
            ->setFirstResult(0);

        $lowLimit = $this->pricesRepo->matching($criteriaLow);

        $criteriaHigh = Criteria::create()
            ->where(Criteria::expr()->gte("periodTill", $endDate))
            ->setMaxResults(1)
            ->setFirstResult(0);

        $highLimit = $this->pricesRepo->matching($criteriaHigh);

        $criteriaPrices = Criteria::create()
            ->where(Criteria::expr()->gte("periodFrom", $lowLimit[0]->getPeriodFrom()));

        if (!is_null($highLimit[0])) {
            $criteriaPrices->andWhere(Criteria::expr()->lte("periodTill", $highLimit[0]->getPeriodTill()));
        }

        $this->pricesIntervals = $this->pricesRepo->matching($criteriaPrices);

        return $this->pricesIntervals;
    }
}
