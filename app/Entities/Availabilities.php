<?php
namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Availabilities
 *
 * @ORM\Table(name="availabilities", indexes={@ORM\Index(name="property_id", columns={"property_id"})})
 * @ORM\Entity
 */
class Availabilities
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="property_id", type="guid", length=36, nullable=false, options={"fixed"=true})
     */
    private $propertyId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @var bool
     *
     * @ORM\Column(name="arrival_allowed", type="boolean", nullable=false)
     */
    private $arrivalAllowed;

    /**
     * @var bool
     *
     * @ORM\Column(name="departure_allowed", type="boolean", nullable=false)
     */
    private $departureAllowed;

    /**
     * @var int
     *
     * @ORM\Column(name="minimum_stay", type="integer", nullable=false)
     */
    private $minimumStay;

    /**
     * @var int
     *
     * @ORM\Column(name="maximum_stay", type="integer", nullable=false)
     */
    private $maximumStay;

    /**
     * @var int
     *
     * @ORM\Column(name="version", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $version;

    public function setDate(\DateTimeInterface $date)
    {
        $this->date = $date;

        return $this;
    }

    public function setArrivalAllowed($allowed)
    {
        $this->arrivalAllowed = (bool)$allowed;

        return $this;
    }

    public function setDepartureAllowed($allowed)
    {
        $this->departureAllowed = (bool)$allowed;

        return $this;
    }

    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function setMinStay(int $stay)
    {
        $this->minimumStay = $stay;

        return $this;
    }

    public function setMaxStay(int $stay)
    {
        $this->maximumStay = $stay;

        return $this;
    }

    public function getPropertyId()
    {
        return $this->propertyId;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function isAllowed()
    {
        return ($this->quantity != 0) && $this->arrivalAllowed;
    }

    public function getMinStay()
    {
        return $this->minimumStay;
    }

    public function getMaxStay()
    {
        return $this->maximumStay;
    }

    public function isDepartureAllowed()
    {
        return $this->departureAllowed;
    }
}
