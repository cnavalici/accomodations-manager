<?php
namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prices
 *
 * @ORM\Table(name="prices", indexes={@ORM\Index(name="property_id", columns={"property_id"})})
 * @ORM\Entity
 */
class Prices
{
    const DELIMITER = '|';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="property_id", type="guid", length=36, nullable=false, options={"fixed"=true})
     */
    private $propertyId;

    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer", nullable=false)
     */
    private $duration;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer", nullable=false)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=3, nullable=false)
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(name="persons", type="string", length=255, nullable=false)
     */
    private $persons = '';

    /**
     * @var string
     *
     * @ORM\Column(name="weekdays", type="string", length=255, nullable=false)
     */
    private $weekdays = '';

    /**
     * @var int
     *
     * @ORM\Column(name="minimum_stay", type="integer", nullable=false)
     */
    private $minimumStay;

    /**
     * @var int
     *
     * @ORM\Column(name="maximum_stay", type="integer", nullable=false)
     */
    private $maximumStay;

    /**
     * @var int
     *
     * @ORM\Column(name="extra_person_price", type="integer", nullable=false)
     */
    private $extraPersonPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="extra_person_price_currency", type="string", length=3, nullable=false)
     */
    private $extraPersonPriceCurrency;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="period_from", type="date", nullable=false)
     */
    private $periodFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="period_till", type="date", nullable=false)
     */
    private $periodTill;

    /**
     * @var int
     *
     * @ORM\Column(name="version", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $version;

    public function getPeriodFrom()
    {
        return $this->periodFrom;
    }

    public function setPeriodFrom(\DateTimeInterface $date)
    {
        $this->periodFrom = $date;

        return $this;
    }

    public function getMinimumStay()
    {
        return $this->minimumStay;
    }

    public function getPeriodTill()
    {
        return $this->periodTill;
    }

    public function setPeriodTill(\DateTimeInterface $date)
    {
        $this->periodTill = $date;

        return $this;
    }

    public function getWeekdays()
    {
        return explode(self::DELIMITER, $this->weekdays);
    }

    public function setWeekdays(array $weekdays)
    {
        $this->weekdays = implode(self::DELIMITER, $weekdays);

        return $this;
    }

    public function getPersons()
    {
        return explode(self::DELIMITER, $this->persons);
    }

    public function setPersons(array $persons)
    {
        $this->persons = implode(self::DELIMITER, $persons);

        return $this;
    }

    public function setDuration(int $duration)
    {
        $this->duration = $duration;

        return $this;
    }

    public function containsDate(\DateTimeInterface $date)
    {
        return $this->periodFrom <= $date && $date <= $this->periodTill;
    }

    public function allowsByPersons(int $personsNo)
    {
        return in_array($personsNo, $this->getPersons());
    }

    public function allowsByWeekday(\DateTimeInterface $date)
    {
        return in_array($date->format('w'), $this->getWeekdays());
    }

    public function allowsByStay(int $daysNo)
    {
        return $this->minimumStay <= $daysNo && $daysNo <= $this->maximumStay;
    }

    public function isExactDuration(int $givenDuration): bool
    {
        return !($givenDuration % $this->duration);
    }

    public function toArray()
    {
        return [
            'weekdays' => $this->getWeekdays(),
            'periodFrom' => $this->periodFrom,
            'periodTill' => $this->periodTill,
            'persons' => $this->getPersons()
        ];
    }

    public function setAmount(int $amount)
    {
        $this->amount = $amount;

        return $this;
    }

    public function setExtraPersonPrice(int $amount)
    {
        $this->extraPersonPrice = $amount;

        return $this;
    }

    public function getAmountForPersons(int $persons): int
    {
        if (!$this->allowsByPersons($persons)) {
            return 0;
        }

        $firstPersons = $this->getPersons()[0];
        $firstPrice = $firstPersons * $this->amount;
        $extra = ($persons - $firstPersons) * $this->extraPersonPrice;

        return $firstPrice + $extra;
    }
}
