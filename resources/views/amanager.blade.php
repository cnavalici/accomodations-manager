<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Accomodation Manager</title>

    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .position-ref {
            position: relative;
        }

        table.greyGridTable {
            border: 2px solid #FFFFFF;
            width: 100%;
            text-align: center;
            border-collapse: collapse;
        }
        table.greyGridTable td, table.greyGridTable th {
            border: 1px solid #FFFFFF;
            padding: 3px 4px;
        }
        table.greyGridTable tbody td {
            font-size: 13px;
        }
        table.greyGridTable td:nth-child(even) {
            background: #EBEBEB;
        }
        table.greyGridTable thead {
            background: #FFFFFF;
            border-bottom: 4px solid #333333;
        }
        table.greyGridTable thead th {
            font-size: 15px;
            font-weight: bold;
            color: #333333;
            text-align: center;
            border-left: 2px solid #333333;
        }
        table.greyGridTable thead th:first-child {
            border-left: none;
        }

        table.greyGridTable tfoot {
            font-size: 14px;
            font-weight: bold;
            color: #333333;
            border-top: 4px solid #333333;
        }
        table.greyGridTable tfoot td {
            font-size: 14px;
        }
    </style>
</head>
<body>
    <div class="position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                Pricing Schema
            </div>

            <table class="greyGridTable">
                <thead>
                <tr>
                    <th>Date/Nights</th>
                    <th>P</th>
                    @for ($d = 1; $d <= 21; $d++)
                        <th>{{ $d }}</th>
                    @endfor
                </tr>
                </thead>

                <tbody>
                @foreach ($results as $date => $prices)
                @for ($pers = 1; $pers <= 3; $pers++)
                    <tr>
                        <td>{{ $date }}</td>
                        <td>{{ $pers }}</td>
                        @foreach($prices as $priceSet)
                        <td>{{ sprintf("%02.2f", $priceSet[$pers]/100) }}</td>
                        @endforeach
                    </tr>
                @endfor
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</body>